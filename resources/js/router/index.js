import {createRouter, createWebHistory} from "vue-router";
import CompaniesIndex from "../components/companies/CompaniesIndex";
import CompaniesCreate from "../components/companies/CompaniesCreate";
import CompaniesEdit from "../components/companies/CompaniesEdit";

const routes = [
    {
        path: '/dashboard',
        name: 'companies.index',
        component: CompaniesIndex
    },
    {
        path: '/company/create',
        name: 'companies.create',
        component: CompaniesCreate
    },
    {
        path: '/company/:id/edit',
        name: 'companies.edit',
        component: CompaniesEdit,
        props: true
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
