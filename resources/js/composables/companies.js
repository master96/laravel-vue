import {ref} from "vue";
import axios from "axios";
import router from "../router";

export default function useCompanies() {
    const companies = ref([])
    const company = ref('')
    const errors = ref('')

    const getCompanies = async () => {
        let response = await axios.get('/api/company')
        companies.value = response.data.data;
    }
    const getCompany = async (id) => {
        let response = await axios.get('/api/company/' + id)
        company.value = response.data.data;
    }
    const destroyCompanies = async (id) => {
       await axios.delete('/api/company/' + id)
    }

    const createCompanies = async (request) => {
        errors.value = '';
        try {
            await axios.post('/api/company', request)
            await router.push({name: 'companies.index'})
        } catch (e) {
            if(e.response.status === 422) {
                for ( const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }
        }

    }

    const updateCompanies = async (id) => {
        errors.value = '';
        try {
            await axios.put('/api/company/' + id, company.value)
            await router.push({name: 'companies.index'})
        } catch (e) {
            if(e.response.status === 422) {
                for ( const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }
        }

    }

    return {
        companies,
        company,
        errors,
        getCompanies,
        getCompany,
        destroyCompanies,
        createCompanies,
        updateCompanies
    }
}
